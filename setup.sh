#!/bin/bash
# This file will be used to setup the project files and check in to GIT.

# Check if a .git directory exists if so delete it
if [ -d .git ]
then
  read -rp "Do you want to remove the existing git repo? (Y/n) " remove_git
  if [ "$remove_git" = "Y" ] || [ "$remove_git" = "y" ]
  then
    echo "Removing existing .git directory"
    rm -fr .git
  fi
fi
# Check if git is installed
if ! command -v git &> /dev/null
then
  # If it's not ask if it should be
  install_git=$(read -r "You must have git installed do you want to install it now? (Y/n) ")
  if [ "$install_git" = "Y" ] || [ "$install_git" = "y" ]
  then
    apt install -y git
  fi
fi
read -rp "Do you want to create a new git repo now? (Y/n) " create_repo
if [ "$create_repo" = "Y" ] || [ "$create_repo" = "y" ]
then
  read -rp "Are you using Github or Gitlab? " git_type_input
  git_type=$(echo "$git_type_input" | tr A-Z a-z)
  if [ "$git_type" = "github" ]
  then
    git_url="https://github.com"
  else
    git_url="https://gitlab.com"
  fi
  read -rp "What is your git username? " git_username
  read -rp "What would you like the repo to be named? " repo_name
  repo_name_formatted=$(echo "$repo_name" | tr A-Z a-z | tr -s ' ' | tr ' ' '_')
  read -rp "What is your initial commit message? " commit_message
  git init
  git remote add origin "$(echo "$git_url/$git_username/$repo_name_formatted.git")"
  git add .
  git commit -m "$commit_message"
  git push -u origin master
  echo "The git repo has been pushed"
fi
# Configure .env file
if [ -f .env ]
then
  read -rp "Do you want to create a new .env file? (Y/n) " create_new_env
else
  create_new_env="Y"
fi
if [ "$create_new_env" = "Y" ] || [ "$create_new_env" = "y" ]
then
  if [ -z "$repo_name" ]
  then
    read -rp "What is your project named? " repo_name
    repo_name_formatted=$(echo "$repo_name" | tr A-Z a-z | tr -s ' ' | tr ' ' '_')
  fi
  read -rp "What is the database name? " database_name
  read -rp "Would you like to randomize the database root password? (Y/n) " use_random_root_password
  if [ "$use_random_root_password" = "Y" ] || [ "$use_random_root_password" = "y" ]
  then
    root_password=$(tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c 13  ; echo)
  else
    read -rp "Please enter a user password for the database " root_password
  fi
  read -rp "What is the database user? " database_user
  read -rp "Would you like to randomize the database users password? (Y/n) " use_random_password
  if [ "$use_random_password" = "Y" ] || [ "$use_random_password" = "y" ]
  then
    database_password=$(tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c 13  ; echo)
  else
    read -rp "Please enter a user password for the database " database_password
  fi
  interface=$(ip -o -4 route show to default | awk '{print $5}')
  ip_address=$(/sbin/ip -o -4 addr list "$interface" | awk '{print $4}' | cut -d/ -f1)
  read -rp "Would you like to create a default web user? (Y/n) " create_default_user
  if [ "$create_default_user" = "Y" ] || [ "$create_default_user" = "y" ]
  then
    create_web_user="true"
    read -rp "What is the default username? " default_username
    read -rp "Would you like to to use a randomized password? (Y/n)" use_random_web_password
    if [ "$use_random_web_password" = "Y" ] || [ "$use_random_web_password" = "y" ]
    then
      default_password=$(tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c 13  ; echo)
    else
      read -rp "What is the default password? " default_password
    fi
    read -rp "What is the default users first name? " default_user_first_name
    read -rp "What is the default users last name? " default_user_last_name
    default_user_fullname="${default_user_first_name}_${default_user_last_name}"
  else
    create_web_user="false"
  fi
  if [ -f .env ]
  then
    rm .env
  fi
  touch .env
  cat > .env <<EOF
APP_NAME=$repo_name_formatted
ENV_TYPE=dev
DB_UPGRADE=true
DB_UPGRADE_MSG=Initial
MYSQL_ROOT_PASSWORD=$root_password
MYSQL_DATABASE=$database_name
MYSQL_USER=$database_user
MYSQL_PASSWORD=$database_password
MYSQL_IP=$ip_address
CREATE_DEFAULT_USER=$create_web_user
DEFAULT_USERNAME=$default_username
DEFAULT_PASSWORD=$default_password
DEFAULT_USER=$default_user_fullname
EOF
fi