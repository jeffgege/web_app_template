## Web App Template
This will be used as a template for all my web applications. It consists of a few different pre-built modules. Including authentication, database configuration, initial user setup, and initialization of different modules.

### Setup
* Clone the project from the git repo ```git clone https://gitlab.com/jeffgege/web_app_template.git```
* Run the setup.sh script and answer the prompts ```chmod +x setup.sh && ./setup.sh```
* Once the project has been created bring up the template using docker-compose ```docker-compose up -d --build```
* You can view the logs using the project name which can be found in the .env file. 
* Once the app is started you can login to check that everything is working. 