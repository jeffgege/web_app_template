import click
from click import pass_context
from flask.cli import AppGroup, with_appcontext
from flask import current_app
from alembic import command
from alembic.config import Config
from app import db
from flask_migrate import Migrate
import os
from app.models import User
from werkzeug.security import generate_password_hash

db_cli = AppGroup('db', help='Various database management commands.')
basedir = os.path.abspath(os.path.dirname(__file__))
migrations_dir = os.path.join(basedir, '/app/migrations')
config_file = os.path.join(basedir, '/app/migrations', 'alembic.ini')


@db_cli.command('init')
def db_init(directory=migrations_dir, multidb=False):
    """Creates a new migration repository"""
    if directory is None:
        directory = current_app.extensions['migrate'].directory
    config = Config()
    config.set_main_option('script_location', directory)
    config.config_file_name = os.path.join(directory, 'alembic.ini')
    config = current_app.extensions['migrate'].\
        migrate.call_configure_callbacks(config)
    if multidb:
        command.init(config, directory, 'flask-multidb')
    else:
        command.init(config, directory, 'flask')


@db_cli.command('migrate')
def db_migrate(message="No Message"):
    """Migrate with alembic."""
    config = Config(config_file)
    command.revision(config, message, autogenerate=True, head='head', sql=False, splice=False)
    click.echo("Database has been migrated")


@db_cli.command('db_upgrade')
@pass_context
def db_upgrade(ctx):
    config = Config(config_file)
    command.upgrade(config, revision='head', sql=False, tag=None)
    click.echo("Database has been upgraded")


@db_cli.command('reset_db')
def reset_db():
    """Drop the database."""
    db.engine.execute("SET FOREIGN_KEY_CHECKS=0;")
    db.drop_all()
    db.engine.execute("SET FOREIGN_KEY_CHECKS=1;")
    click.echo("Drop all tables.")
