#!/bin/bash
# clear screen
clear
# Check for config file
if [ ! -f /app/config.py ]
then
  echo "You must create a config file in this directory. Please copy the config.py.sample and fill in the information"
  exit 1
fi
ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
# Install the Python requirements
pip install -r requirements.txt
# Check if DB upgrade ENV variable is set to true
if [ "$DB_UPGRADE" = "true" ]
then
  # If it is preform a DB upgrade
  echo "Upgrading database"
  flask db init
  flask db migrate -m "$DB_UPGRADE_MSG"
  flask db upgrade
  echo "Database upgrade completed"
fi
# Check the environment type
if [ "$ENV_TYPE" = "prod" ]
then
  # If prod use Gunicorn instead of Flask builtin HTTP server
  gunicorn --config /app/gunicorn_config.py run:app --log-file /app/logs/gunicorn.log
else
  # Otherwise use the builtin HTTP server
  python /app/run.py run
fi