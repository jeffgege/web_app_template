##V1.0
* Added changelog
* Updated setup.sh script to read/generate different mysql root password
* Updated README
* Updated setup.sh script to use the formatted repo name in the .env file