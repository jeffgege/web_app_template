import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login
from datetime import datetime, timedelta
from sqlalchemy.orm import relationship, column_property
import base64
from app.utils.utils import force_logout


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    fullname = db.column_property(first_name+" "+last_name)
    password_hash = db.Column(db.String(120), index=True)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    email = db.Column(db.String(120), index=True, unique=True)
    default_organization = db.Column(db.Integer, db.ForeignKey('organizations.id'), index=True)
    organization = db.Column(db.String(64))
    security_question = db.Column(db.Integer, db.ForeignKey('security_questions.id'))
    security_answer = db.Column(db.String(120))
    show_message = db.Column(db.Boolean)
    new_user = db.Column(db.Boolean)
    permission = db.Column(db.Integer)
    date_created = db.Column(db.DateTime)
    # Last seen is a datetime set at login and logout
    last_seen = db.Column(db.DateTime)
    # Last active checks session activeness
    last_active = db.Column(db.DateTime)
    current_page = db.Column(db.String(120))
    logged_in = db.Column(db.Integer)
    failed_login = db.Column(db.Integer)
    enabled = db.Column(db.Boolean)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        message = ""
        if check_password_hash(self.password_hash, password):
            return True
        else:
            failed_logins = self.failed_login + 1
            self.failed_login = failed_logins
            message = "Invalid username or password"
        if failed_logins > current_app.config['FAILED_LOGIN_MAX'] or self.status is False:
            self.enabled = False
            message = "Your account has been locked due to unsuccessful login attempts. Please contact support"
        return message


    def get_reset_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('UTF-8')

    def is_enabled(self):
        return self.enabled

    def get_permission(self):
        return self.permission

    @staticmethod
    def verify_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithm=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def is_admin(self):
        if int(self.permission) == 1:
            return True
        else:
            return False

    def get_users_name(self):
        return self.first_name+" "+self.last_name

    def checked_is_logged_in(self):
        current_time = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
        inactive = datetime.strptime(str(self.last_active), '%Y-%m-%d %H:%M:%S') + timedelta(minutes=current_app.config['INACTIVITY_THRESHOLD'])
        if self.logged_in == 0 or self.logged_in is False:
            return False
        if inactive <= current_time:
            current_app.logger.info("Inactive Time"+str(inactive)+"\tCurrent Time"+str(current_time))
            force_logout()
            return False
        return True

    def update_user_activity(self, page):
        if self.checked_is_logged_in():
            self.last_active = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.current_page = page
            db.session.add(self)
            db.session.commit()
            return True
        else:
            self.logged_in = 0
            return False

    def set_logout(self):
        self.last_seen = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.logged_in = 0
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Organizations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True)
    status = db.Column(db.Boolean)


class SecurityQuestions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(64), index=True)
    status = db.Column(db.Boolean)