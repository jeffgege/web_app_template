from flask import render_template, redirect, url_for, flash, session, current_app
from flask_login import login_user, logout_user, current_user, login_required
from app.auth import bp
from app.auth.forms import LoginForm, TermsConditionsForm, ResetPasswordForm, ResetPasswordRequestForm
from datetime import datetime
from app import db
from app.models import User
from sqlalchemy import exc
import os


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    if os.environ['CREATE_DEFAULT_USER'] == "true":
        try:
            if not User.query.filter_by(username=os.environ['DEFAULT_USERNAME']).first() is None:
                pass
            else:
                default_user = User()
                default_user.username = os.environ['DEFAULT_USERNAME']
                default_user.set_password(os.environ['DEFAULT_PASSWORD'])
                user_first_name, user_last_name = os.environ['DEFAULT_USER'].split('_', 1)
                default_user.first_name = user_first_name
                default_user.last_name = user_last_name
                default_user.date_created = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                db.session.add(default_user)
                db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to create initial user")
            render_template("error/startup_error.html", title="Fatal Error")
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None:
            flash('Invalid username or password', 'danger')
            return redirect(url_for('auth.login'))
        check_pass = user.check_password(form.password.data)
        if check_pass is not True:
            flash(check_pass)
            return redirect(url_for('auth.login'))
        login_user(user)
        session['user_id'] = current_user.get_id()
        session['username'] = form.username.data
        session['first_name'] = user.first_name
        session['last_name'] = user.last_name
        session['permission'] = user.permission
        session['current_org'] = user.default_organization
        try:
            user.logged_in = 1
            user.last_active = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            user.last_seen = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            db.session.add(user)
            db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to update users login data")
            flash("There was an error logging in. Please try again or contact support", 'danger')
            return redirect(url_for('auth.login'))
        if user.new_user == 1:
            # redirect to ToC page
            return redirect(url_for('main.index'))
        elif user.show_message == 1:
            # redirect to message window
            return redirect(url_for('main.index'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/login.html', title='Login', form=form)


@login_required
@bp.route('/logout', methods=['GET', 'POST'])
def logout():
    current_user.set_logout()
    logout_user()
    return redirect(url_for('auth.login'))


@login_required
@bp.route("/terms_and_conditions", methods=['GET', 'POST'])
def terms_and_conditions():
    form = TermsConditionsForm()
    return render_template('auth/login.html', title='Login', form=form)