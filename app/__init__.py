import logging
from logging.handlers import RotatingFileHandler
import os
from flask import Flask, request, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_migrate import Migrate
from config import config_by_name

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = "Please login to view this page"
bootstrap = Bootstrap()
moment = Moment()


def create_app(config_type):
    try:
        if config_type.lower() != os.getenv('ENV_TYPE') or 'dev':
            config_type = os.getenv('ENV_TYPE') or 'dev'
    except AttributeError:
        config_type = os.getenv('ENV_TYPE') or 'dev'
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_type])
    if app.config["DEBUG"] is True:
        app.debug = True
    app.host = "0.0.0.0"

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    log_level = logging.INFO
    if app.config['LOG_TO_STDOUT'] is True:
        stream_handler = logging.StreamHandler()
        if app.config['DEBUG'] is True:
            log_level = logging.DEBUG
            stream_handler.setLevel(log_level)
        else:
            stream_handler.setLevel(logging.INFO)
        if not app.logger.handlers:
            log_level = logging.INFO
            stream_handler.setLevel(log_level)
    else:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('/app/logs/security_suite.log',
                                           maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'))
        if app.config['DEBUG'] is True:
            log_level = logging.DEBUG
            file_handler.setLevel(log_level)
        else:
            log_level = logging.INFO
            file_handler.setLevel(log_level)
        for handler in app.logger.handlers:
            app.logger.removeHandler(handler)
        app.logger.addHandler(file_handler)
    app.logger.propagate = False
    app.logger.setLevel(log_level)
    app.logger.info(os.environ['APP_NAME']+' Starting in ' + config_type + ' mode')
    return app
from app import models