from flask import render_template, redirect, url_for, flash, session, current_app
from flask_login import logout_user, current_user
from app import db
import json


def force_logout():
    flash("Your session has expired please login again.")
    current_user.set_logout()
    logout_user()
    redirect(url_for('auth.login'))




