from flask import render_template, redirect, url_for, flash, session, current_app, request
from flask_login import login_user, logout_user, current_user, login_required
from app.main import bp
from app.utils.utils import force_logout
import json


@bp.before_request
def check_authenticated():
    try:
        if not current_user.is_authenticated:
            return force_logout()
        if not current_user.checked_is_logged_in():
            return force_logout()
    except AttributeError:
        redirect(url_for('auth.login'))


@bp.route('/', methods=['GET'])
@bp.route('/index', methods=['GET'])
@login_required
def index():
    return "Hello, looks like everything is working. Time to start building!"
