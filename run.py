import os
import unittest
from app import create_app, db

app = create_app(os.getenv('ENV_TYPE') or 'dev')
app.app_context().push()


def run():
    app.run(host="0.0.0.0")


def test():
    tests = unittest.TestLoader().discover('test', pattern='test*.py')
    results = unittest.TextTestRunner(verbosity=2).run(tests)
    if results.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    app.run(host="0.0.0.0")
