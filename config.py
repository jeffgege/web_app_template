import os
import datetime
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    BASE_DIR = basedir
    SECRET_KEY = os.urandom(32)
    TIMESTAMP = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    DATE = datetime.datetime.now().strftime('%Y-%m-%d')


class DevelopmentConfig(Config):
    DEBUG = True
    LOG_TO_STDOUT = True
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://" + os.environ['MYSQL_USER'] + ":" + os.environ['MYSQL_PASSWORD'] + "@" + \
                              os.environ['MYSQL_IP'] + "/" + os.environ['MYSQL_DATABASE']
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    INACTIVITY_THRESHOLD = 10
    FAILED_LOGIN_MAX = 5


class TestingConfig(Config):
    DEBUG = True
    LOG_TO_STDOUT = True
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://" + os.environ['MYSQL_USER'] + ":" + os.environ['MYSQL_PASSWORD'] + "@" + \
                              os.environ['MYSQL_IP'] + "/" + os.environ['MYSQL_DATABASE']
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProdConfig(Config):
    DEBUG = False
    LOG_TO_STDOUT = False
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://" + os.environ['MYSQL_USER'] + ":" + os.environ['MYSQL_PASSWORD'] + "@" + \
                              os.environ['MYSQL_IP'] + "/" + os.environ['MYSQL_DATABASE']
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProdConfig
)
