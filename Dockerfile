FROM python:3.8

RUN apt-get update -y && apt-get -y upgrade && apt-get -y install unixodbc unixodbc-dev dos2unix

COPY . /app

WORKDIR /app

RUN chmod +x /app/resources/start.sh
RUN dos2unix /app/resources/start.sh